# Abschlussprojekt ILV Automatisierung Teil2 WS2021/22
## Philipp Szechenyi

1. Git Repository wurde initialisiert, ein neuer branch erstellt (container), und der Inhalt von README.md auf dem branch main wurde gelöscht, um Platz für dieses Dokument zu schaffen
2. Auf dem branch container wurde ein Dockerfile erstellt, das Grundimage ist debian:bullseye-slim, Labels wurden hinzugefügt, und die benötigten Programminstallationen incl. apt-get update wurden hinzugefügt
3. Samtools über ADD hinzugefügt, mittels WORKDIR entpackt und der make Befehl wird ausgeführt.
4. Entry directory hinzugefügt, Test mit der *.bam file, Ergebnis: 1215970
5. Merge container in main
6. Tag 1.0 hinzufügen
7. pushen und somit ist das Projekt fertig :)
